/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author salasistemas
 */
public class Agente extends Persona {

    private short numeroPlaca;

    public Agente(short numeroPlaca, long identificacion, String nombre, String apellido) throws Exception {
        super(identificacion, nombre, apellido);
        if (numeroPlaca > 99999 || numeroPlaca < 000001) {
            throw new Exception("El numeroPlaca no puede tener mas de 5 digitos numericos");
        }
        this.numeroPlaca = numeroPlaca;
    }

    public short getNumeroPlaca() {
        return numeroPlaca;
    }

}
