/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.util.Date;

/**
 *
 * @author salasistemas
 */
public class Licencia {
    
    private Date fecha;
    private Persona persona;
    private Categoria categoria;
   

    public Licencia(Persona persona, Categoria categoria, Date fecha) {
        this.persona = persona;
        this.categoria = categoria;
        this.fecha = fecha;
    }

    public Persona getPersona() {
        return persona;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public Date getFecha() {
        return fecha;
    }
    
    
    
}
