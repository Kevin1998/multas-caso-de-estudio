/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import com.sun.org.apache.xpath.internal.operations.Mult;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author salasistemas
 */
public class OrganismoTransito {

    private LinkedList<Licencia> licencias;
    private LinkedList<Agente> agentes;
    private LinkedList<MotivoMulta> motivosmultas;
    private LinkedList<Multa> multas;
    private LinkedList<Vehiculo> vehiculos;

    public OrganismoTransito() {
        this.licencias = new LinkedList();
        this.agentes = new LinkedList();
        this.motivosmultas = new LinkedList();
        this.multas = new LinkedList();
        this.vehiculos = new LinkedList();
    }

    public void agregarAgente(Agente agente) {
        this.agentes.add(agente);
    }

    public void agregarLicencia(Licencia licencia) {
        this.licencias.add(licencia);
    }

   // public void agregarMotivoMulta(MotivoMulta motivomulta) {
    //    this.motivosmultas.add(motivomulta);
   // }
    //////////////////////////////////////////////////////////////////
    public void removerLicencia(Licencia licencia) {
        this.licencias.remove(licencia);
    }

    public void removerAgente(Agente agente) {
        this.agentes.remove(agente);
    }

    public void removerMotivoMulta(MotivoMulta motivomulta) {
        this.motivosmultas.remove(motivomulta);
    }

    public void removerMulta(Multa multa) {
        this.multas.remove(multa);
    }

    public void removerVehiculo(Vehiculo vehiculo) {
        this.vehiculos.remove(vehiculo);
    }

//////////////////////////////////////////////////////////////////////////////////
    public void agregarVehiculo(Vehiculo vehi) throws Exception {
        for (Vehiculo vehiculo : this.vehiculos) {
            if (vehi.getPlaca().equals(vehiculo.getPlaca())) {
                throw new Exception("El vehiculo ya esta agregado con placa " + vehiculo.getPlaca());
            }
        }
        this.vehiculos.add(vehi);

    }

      public void agregarMotivoMulta(MotivoMulta codigo) throws Exception {
        for (MotivoMulta motivomulta : this.motivosmultas) {
            if (codigo.getCodigo() == motivomulta.getCodigo()) {
                throw new Exception("Este motivo multa ya fue agreado ");  
            }
        }
         this.motivosmultas.add(codigo);
    }
    public void agregarMulta(Multa identificacion) throws Exception {
        for (Multa multa : this.multas) {
            if (identificacion.getPersona().getIdentificacion() == multa.getPersona().getIdentificacion()) {
                throw new Exception("La multa con esta identificacion ya esta agregada");
            }
        }
        this.multas.add(identificacion);
    }

    public Vehiculo buscarVehiculo(String placa) throws Exception {
        for (Vehiculo vehiculo : this.vehiculos) {
            if (placa.equals(vehiculo.getPlaca())) {
                return vehiculo;
            }
        }
        throw new Exception("No existe vehiculo con esa dicha placa");
    }

    public Licencia buscarLicencia(long identificacion) throws Exception {
        for (Licencia licencia : this.licencias) {
            if (identificacion == licencia.getPersona().getIdentificacion()) {
                return licencia;
            }
        }
        throw new Exception("No existe licencia con esta identificacion");
    }

    public List<Licencia> buscarLicencias(long identificacion) {

        LinkedList<Licencia> encontradas = new LinkedList<>();
        for (Licencia licencia : this.licencias) {
            if (licencia.getPersona().getIdentificacion() == identificacion) {
                encontradas.add(licencia);
            }
        }
        return encontradas;
    }
    
     public List<MotivoMulta> buscarMotivoMultas(short codigo) throws Exception {
        for (MotivoMulta motivomulta : this.motivosmultas) {
            if (codigo == motivomulta.getCodigo()) {
                this.motivosmultas.add(motivomulta);
                return (List<MotivoMulta>) motivomulta;
            }
        }
        throw new Exception("No existe un motivo multa con ese codigo");
    }

 

    public Agente buscarAgente(short placa) throws Exception {
        for (Agente agente : this.agentes) {
            if (placa == agente.getNumeroPlaca()) {
                return agente;
            }
        }
        throw new Exception("No existe agente con esta placa");
    }

    public Multa buscarMulta(long identificacion) throws Exception {
        for (Multa multa : this.multas) {
            if (identificacion == multa.getPersona().getIdentificacion()) {
                return multa;
            }
        }
        throw new Exception("No existe multa con esta identificaciones");
    }

    public MotivoMulta buscarMotivoMulta(short codigo) throws Exception {
        for (MotivoMulta motivomulta : this.motivosmultas) {
            if (codigo == motivomulta.getCodigo()) {
                return motivomulta;
            }

        }
        throw new Exception("No existe ese motivo multa con ese codigo");
    }

}
