/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Jose Kevin Estupiñan Caicedo
 */
public class MotivoMulta {

    private short codigo;
    private String descripcion;
    private int valor;

    public MotivoMulta(short codigo, String descripcion, int valor) throws Exception {
        
        if (descripcion == null || "".equals(descripcion.trim())){
            throw new Exception("La descripcion no puede ser nulo o vacio");}
            
        if (codigo > 999999 || codigo < 000001){  
            throw  new Exception("El codigo no puede tener mas de 6 digitos numericos");}
        
        if (valor > 999999999 || valor < 000000001 ){
            throw new Exception("El valor no puede tener mas de 9 digitos numericos");}
         
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.valor = valor;
    }

    public short getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getValor() {
        return valor;
    }

}
