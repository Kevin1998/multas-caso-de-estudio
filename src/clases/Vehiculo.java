/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Jose Kevin Estupiñan Caicedo
 */
public class Vehiculo {
    
    private String placa;
    private short modelo;
    private String color;
    private String marca;

    public Vehiculo(String placa, short modelo, String color, String marca) throws Exception {
        
        if (placa == null || "".equals(placa.trim())) {
            throw new Exception("La placa no puede ser nulo o vacio");
        }
        if (color == null || "".equals(color.trim())) {
            throw new Exception("El color no puede ser nulo o vacio");}
        
        if (marca == null || "".equals(marca.trim())) {
            throw new Exception("La marca no puede ser nulo o vacio");}
        
        if(modelo > 9999 || modelo < 0001 ){
            throw new Exception("El modelo no puede tener mas de 4 digitos numericos");}
       
        this.placa = placa;
        this.modelo = modelo;
        this.color = color;
        this.marca = marca;
    }

    public String getPlaca() {
        return placa;
    }

    
    public short getModelo() {
        return modelo;
    }

 
    public String getColor() {
        return color;
    }


    public String getMarca() {
        return marca;
    }


}
