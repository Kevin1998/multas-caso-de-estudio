/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Jose Kevin Estupiñan Caicedo
 */
public class Persona {
    
    private long identificacion;
    private String nombre;
    private String apellido;

    public Persona(long identificacion, String nombre, String apellido)throws Exception {
        
        if (nombre == null || "".equals(nombre.trim())) {
            throw new Exception("El nombre no puede ser nulo o vacio");}
        
        if (apellido == null || "".equals(apellido.trim())) {
            throw new Exception("El apellido no puede ser nulo o vacio");}
        
        if (identificacion > 99999999999L || identificacion < 00000000001L) {
            throw new Exception("La identificacion no puede ser menor a cero y mayor a 10");}
        
     
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.apellido = apellido;
}

    public long getIdentificacion() {
        return identificacion;
    }

   
    public String getNombre() {
        return nombre;
    }

   
    public String getApellido() {
        return apellido;
    }

   
}
