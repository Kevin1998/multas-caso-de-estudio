/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author salasistemas
 */
public class Multa {
    
    private int valor;
    private LinkedList<MotivoMulta>motivosmultas;
    private Agente agente;
    private Persona persona;
    private Vehiculo vehiculo;
    private Date fecha;

    public Multa(int valor, Agente agente, Persona persona, Vehiculo vehiculo, Date fecha) throws Exception{
        
        if (valor > 999999999 || valor < 000000001 ){
            throw new Exception("El valor no puede tener mas de 9 digitos numericos");}
        
        this.valor = valor;
        this.motivosmultas  = new LinkedList ();
        this.agente = agente;
        this.persona = persona;
        this.vehiculo = vehiculo;
        this.fecha = fecha;
    }

    public int getValor() {
        return valor;
    }

    public LinkedList<MotivoMulta> getMotivosmultas() {
        return motivosmultas;
    }

  
    public Agente getAgente() {
        return agente;
    }

    public Persona getPersona() {
        return persona;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public Date getFecha() {
        return fecha;
    }
    
     
    
}
