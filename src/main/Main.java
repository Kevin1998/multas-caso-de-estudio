/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.Date;
import clases.Agente;
import clases.Categoria;
import clases.Licencia;
import clases.MotivoMulta;
import clases.Multa;
import clases.OrganismoTransito;
import clases.Persona;
import clases.Vehiculo;
import java.text.SimpleDateFormat;
import ui.Principal;

/**
 *
 * @author salasistemas
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {

        OrganismoTransito organismotransito = new OrganismoTransito();

        Date fecha = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/mm/dd");
        fecha = sdf.parse("2016/01/01");

        Persona persona1 = new Persona(1113691743, "Kevin", "Estupiñan");

        Agente agente1 = new Agente((short) 13096, 2765144, "Jose", "Caicedo");

        Licencia licencia1 = new Licencia(persona1, Categoria.A1, fecha);

        MotivoMulta motivomulta1 = new MotivoMulta((short) 13245, "No tiene papeles del vehiculo", 1300000);

        Vehiculo vehiculo1 = new Vehiculo("1520ABC", (short) 2018, "Verde", "Mazda");

        Multa multa1 = new Multa(13000000, agente1, persona1, vehiculo1, fecha);
        
        
        
         Agente[] agentes = {agente1};

        for (Agente agente : agentes) {
            organismotransito.agregarAgente(agente);
        }
        
        Vehiculo[] vehiculos = {vehiculo1};

        for (Vehiculo vehiculo : vehiculos) {
            organismotransito.agregarVehiculo(vehiculo);
        }
        
        Licencia[] licencias = {licencia1};

        for (Licencia licencia : licencias) {
            organismotransito.agregarLicencia(licencia1);
        }
        
        MotivoMulta[] motivomultas = {motivomulta1};

       for (MotivoMulta motivomulta : motivomultas) {
           organismotransito.agregarMotivoMulta(motivomulta);
       }
        
        Multa[] multas = {multa1};

        for (Multa multa : multas) {
            organismotransito.agregarMulta(multa);
        }





        System.out.println(agente1.getNumeroPlaca());
        System.out.println(persona1.getNombre());
        System.out.println(licencia1.getCategoria());
        System.out.println(motivomulta1.getValor());
        System.out.println(vehiculo1.getModelo());
        System.out.println(vehiculo1.getPlaca());
        System.out.println(multa1.getValor());
        System.out.println(multa1.getFecha());

        Principal principal = new Principal(organismotransito);
        principal.setVisible(true);

       
    }

}
